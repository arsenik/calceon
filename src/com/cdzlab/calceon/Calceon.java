package com.cdzlab.calceon;

import com.cdzlab.calceon.action.Action;
import com.cdzlab.calceon.action.AddAction;
import com.cdzlab.calceon.action.MultiAction;
import com.cdzlab.calceon.action.SubtractAction;
import com.cdzlab.calceon.block.Block;
import com.cdzlab.calceon.block.BlockType;

import java.util.HashMap;
import java.util.Stack;

/**
 * @author arsenik
 */
public class Calceon {
    private Stack<Action> mActionsStack = new Stack<Action>();
    private Stack<Integer> mDigitsStack = new Stack<Integer>();
    private HashMap<Character, Action> mActions = new HashMap<Character, Action>();
    private String mExpr;
    private int mCurPosition = 0;
    private BlockType mPrevType;

    public Calceon() {
        initActions();
    }

    private void initActions() {
        mActions.put(MultiAction.ACTION_CHAR, new MultiAction());
        mActions.put(AddAction.ACTION_CHAR, new AddAction());
        mActions.put(SubtractAction.ACTION_CHAR, new SubtractAction());
    }

    private void initExpression(String expr) {
        mExpr = expr;
        mPrevType = BlockType.BEGIN;
    }

    private boolean isEnd() {
        return mCurPosition >= mExpr.length() - 1;
    }

    private BlockType getNextCharBlockType() {
        BlockType nextCharType = BlockType.NONE;
        if (mCurPosition < mExpr.length() - 1) {
            char nextChar = mExpr.charAt(mCurPosition + 1);
            if (Character.isDigit(nextChar)) {
                nextCharType = BlockType.DIGIT;
            } else if (isCharAction(nextChar)) {
                nextCharType = BlockType.ACTION;
            }
        }
        return nextCharType;
    }

    private Block nextBlock() {
        StringBuilder sbBuffer = new StringBuilder();
        Block block = Block.getEndBlock();
        while (mCurPosition < (mExpr.length() - 1)) {
            char c = mExpr.charAt(mCurPosition);
            mCurPosition++;
            if (Character.isDigit(c)) {
                sbBuffer.append(c);
                System.out.println("append: " + c);
                if (getNextCharBlockType() != BlockType.DIGIT) {
                    System.out.println("digit: " + sbBuffer.toString());
                    block = new Block(BlockType.DIGIT, Integer.valueOf(sbBuffer.toString()));
                    mPrevType = BlockType.DIGIT;
                    break;
                } else {
                    mPrevType = BlockType.DIGIT;
                }
            } else if (isCharAction(c)) {
                sbBuffer.append(c);
                if (getNextCharBlockType() != BlockType.ACTION) {
                    System.out.println("action: " + c);
                    block = new Block(BlockType.ACTION, mActions.get(c));
                    mPrevType = BlockType.ACTION;
                    break;
                } else {
                    mPrevType = BlockType.ACTION;
                }
            }
        }

        return block;
    }

    public int calc(String expr) {
        initExpression(expr);

        int result = 0;
        while (!isEnd()) {
            Block block = nextBlock();

            if (block.getType() == BlockType.DIGIT) {
                mDigitsStack.push(block.getInt());
            } else if (block.getType() == BlockType.ACTION) {
                Action action = block.getAction();
                mActionsStack.push(action);
            }
        }

        while(!mActionsStack.isEmpty()) {
            int a = mDigitsStack.pop();

            Action action = mActionsStack.pop();
            result = action.make(a, result);
        }

        return result;
    }

    private boolean isCharDigit(char c) {
        int digit = charToDigit(c);
        if (digit >= 0 && digit <= 9) {
            return true;
        }
        return false;
    }

    private boolean isCharAction(char c) {
        return mActions.containsKey(Character.valueOf(c));
    }

    private int charToDigit(char c) {
        final int digit = c - 48;
        return digit;
    }
}
