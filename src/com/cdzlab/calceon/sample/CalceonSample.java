package com.cdzlab.calceon.sample;

import com.cdzlab.calceon.Calceon;

/**
 * @author arsenik
 */
public class CalceonSample {
    public static void main(String[] args) {
        System.out.println("Calceon");
        Calceon calceon = new Calceon();
        long result = calceon.calc("5+18+3");
        System.out.print("result: " + result);
    }
}
