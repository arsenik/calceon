package com.cdzlab.calceon.block;

public enum BlockType {
        DIGIT, ACTION, BEGIN, END, NONE,
        //TODO add parse newline \n chars
        NEWLINE
    }