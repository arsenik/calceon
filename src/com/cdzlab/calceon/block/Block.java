package com.cdzlab.calceon.block;

import com.cdzlab.calceon.action.Action;

/**
 * @author arsenik
 */
public class Block {
    private int mInt;
    private Action mAction;
    private BlockType mType = BlockType.NONE;

    public Block(BlockType blockType) {
        mType = blockType;
    }

    public Block(BlockType blockType, Action action) {
        mType = blockType;
        mAction = action;
    }

    public Block(BlockType blockType, int digitInt) {
        mInt = digitInt;
        mType = blockType;
    }

    public BlockType getType() {
        return mType;
    }

    public int getInt() {
        return mInt;
    }

    public Action getAction() {
        return mAction;
    }

    public void setType(BlockType type) {
        mType = type;
    }

    public void setInt(int digitInt) {
        mInt = digitInt;
    }

    public static Block getEndBlock() {
        return new Block(BlockType.END);
    }

    public void setAction(Action action) {
        mAction = action;
    }
}
