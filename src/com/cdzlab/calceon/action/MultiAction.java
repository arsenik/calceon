package com.cdzlab.calceon.action;

/**
 * @author arsenik
 */
public class MultiAction extends Action {
    public static final char ACTION_CHAR = '*';

    @Override
    public char getActionChar() {
        return ACTION_CHAR;
    }

    @Override
    public int make(int a, int b) {
        return a * b;
    }
}
