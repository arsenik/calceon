package com.cdzlab.calceon.action;

/**
 * @author arsenik
 */
public abstract class Action {
    public boolean isActionForChar(char c) {
        return getActionChar() == c;
    }

    public abstract char getActionChar();
    public abstract int make(int a, int b);
}
